from typing import List

import numpy as np
import numpy.typing as npt

from .interfaces import Camera


class MultiCamera(Camera[List[npt.NDArray[np.uint8]]]):
    """Wrap for multi-camera operation.

    Allows you to work simultaneously with several cameras.
    """

    _instance_count: int = 0

    _name: str
    _cameras: List[Camera[npt.NDArray[np.uint8]]]

    @classmethod
    def create(cls, cameras: List[Camera[npt.NDArray[np.uint8]]]) -> "MultiCamera":
        """Create multicamera.

        Args:
            cameras (List[Camera[npt.NDArray[np.uint8]]]): List with cameras
        """
        cls._instance_count += 1
        return cls(cameras, f"MC{cls._instance_count}")

    def __init__(self, cameras: List[Camera[npt.NDArray[np.uint8]]], name: str) -> None:
        super().__init__()
        self._name = name
        self._cameras = cameras

    @property
    def name(self) -> str:
        return self._name

    @property
    def source_name(self) -> str:
        return "Virtual:MultiCamera"

    def is_opened(self) -> bool:
        return all(c.is_opened() for c in self._cameras)

    def open(self) -> None:
        for c in self._cameras:
            c.open()

    def close(self) -> None:
        for c in self._cameras:
            c.close()

    def grab(self) -> None:
        for c in self._cameras:
            c.grab()

    def retrieve(self) -> List[npt.NDArray[np.uint8]]:
        return [c.retrieve() for c in self._cameras]
