import json
import os
import time
from enum import Enum
from threading import Lock, Thread
from typing import Any, Dict, List, Optional, Tuple, TypeVar, Union

import cv2
import numpy as np
import numpy.typing as npt

from .exceptions import FailedToCreateCameraException, FailedToFetchCameraException
from .interfaces import Camera
from .virtual_camera_writer import VirtualCameraWriter

T = TypeVar("T")

_ImageType = Union[npt.NDArray[np.uint8], List[npt.NDArray[np.uint8]]]


class VirtualCamera(Camera[_ImageType]):

    Writer = VirtualCameraWriter

    class LoopMode(Enum):
        LOOP = 0
        PING_PONG = 1
        FREEZE_FRAME = 2

    _instance_count: int = 0

    _working: bool = False
    _folder_path: str
    _target_fps: float
    _loop_mode: LoopMode
    _name: str

    _thread: Thread
    _grab_lock: Lock
    _retrieve_lock: Lock
    _first_grap: bool = True
    _grap_call_time: Optional[int] = None
    _target_frame: int = 0
    _loop_direction: int = 1
    _data_info: Dict[str, Any]
    _worker_res: Tuple[int, Union[str, _ImageType]] = (-1, "empty frame result")  # <0 - error

    @classmethod
    def create(
        cls, folder_path: str, target_fps: float = 0, loop_mode: LoopMode = LoopMode.LOOP, *, freeze_at_frame: int = 0
    ) -> "VirtualCamera":
        """Create virtual vamera.

        Args:
            folder_path (str): path to folder with images
            target_fps (float): target fps. 0 - as fast as possible
            loop_mode (LoopMode): loop mode
            freeze_at_frame (int): A frame to freeze on. Only makes sense in FREEZE_FRAME mode
        """
        cls._instance_count += 1
        return cls(folder_path, target_fps, loop_mode, freeze_at_frame, f"VC{cls._instance_count}")

    def __init__(  # pylint: disable=too-many-arguments
        self, folder_path: str, target_fps: float, loop_mode: LoopMode, freeze_at_frame: int, name: str
    ) -> None:
        super().__init__()
        if target_fps < 0:
            raise ValueError("target_fps cannot be lower than 0")

        self._folder_path = folder_path
        self._target_fps = target_fps
        self._loop_mode = loop_mode
        self._name = name

        # load data_info.json
        filename = os.path.join(self._folder_path, "data_info.json")
        if not os.path.isfile(filename):
            raise FailedToCreateCameraException(f"File with name {filename} not found")
        with open(filename, "r", encoding="utf8") as f:
            self._data_info = json.load(f)
        data_info_cnt = len(self._data_info["data"])
        if data_info_cnt == 0:
            raise FailedToCreateCameraException(f"There are no frames in the file {filename}")

        self._thread = Thread(target=self._worker)
        self._grab_lock = Lock()
        self._retrieve_lock = Lock()
        if self._loop_mode == VirtualCamera.LoopMode.FREEZE_FRAME:
            self._loop_direction = 0
            if (freeze_at_frame >= data_info_cnt) or (-freeze_at_frame > data_info_cnt):
                raise ValueError(f"freeze_at_frame have to be in range [{-data_info_cnt}; {data_info_cnt-1}]")
            self._target_frame = freeze_at_frame % data_info_cnt

    @property
    def name(self) -> str:
        return self._name

    @property
    def source_name(self) -> str:
        return "Virtual:VirtualCamera"

    def is_opened(self) -> bool:
        return self._working

    def open(self) -> None:
        if self._working:
            return
        self._working = True
        self._first_grap = True
        self._grap_call_time = None
        self._retrieve_lock.acquire(blocking=False)  # pylint: disable=consider-using-with
        self._thread.start()

    def close(self) -> None:
        if not self._working:
            return
        self._working = False
        try:
            self._grab_lock.release()
        except RuntimeError:
            pass
        self._thread.join()

        try:
            self._grab_lock.release()
        except RuntimeError:
            pass
        try:
            self._retrieve_lock.release()
        except RuntimeError:
            pass

    def _load_single_frame(self, file_path: str) -> npt.NDArray[np.uint8]:
        if not os.path.isfile(file_path):
            raise RuntimeError(f"File with name {file_path} do not exist")
        return cv2.imread(file_path)

    def _load_frame(self, frame_id: int) -> _ImageType:
        frame_info = self._data_info["data"][frame_id]
        if isinstance(frame_info, list):
            return [self._load_single_frame(os.path.join(self._folder_path, fi["filename"])) for fi in frame_info]
        return self._load_single_frame(os.path.join(self._folder_path, frame_info["filename"]))

    def _worker(self) -> None:
        while True:
            self._grab_lock.acquire()  # pylint: disable=consider-using-with
            if not self._working:
                break

            target_frame = self._target_frame
            if self._worker_res and target_frame == self._worker_res[0]:
                try:
                    self._retrieve_lock.release()
                except RuntimeError:
                    pass
                continue  # nothing to do, frame already loaded

            try:
                self._worker_res = (target_frame, self._load_frame(target_frame))
            except Exception as e:  # pylint: disable=broad-except
                self._worker_res = (-1, repr(e))
            try:
                self._retrieve_lock.release()
            except RuntimeError:
                pass

    def _set_next_frame(self) -> None:
        data_info_cnt = len(self._data_info["data"])

        next_f = self._target_frame + self._loop_direction
        if 0 <= next_f < data_info_cnt:
            self._target_frame = next_f
        elif self._loop_mode == VirtualCamera.LoopMode.LOOP:
            self._target_frame = next_f % data_info_cnt
        elif self._loop_mode == VirtualCamera.LoopMode.PING_PONG:
            self._loop_direction = -self._loop_direction
            self._target_frame += self._loop_direction

    def grab(self) -> None:
        if not self._working:
            raise FailedToFetchCameraException("Failed to grab frame. Did you call .open()?")

        if self._first_grap:  # do not need to cheange target frame at first time
            self._first_grap = False
        else:
            self._set_next_frame()

        self._grap_call_time = time.monotonic_ns()
        try:
            self._grab_lock.release()
        except RuntimeError:
            pass

    def _sleep_for_target_fps(self, grap_call_time: int) -> None:
        if self._target_fps > 0:
            sleep_time = 1 / self._target_fps - (time.monotonic_ns() - grap_call_time) / 1e9
            if sleep_time > 0:
                time.sleep(sleep_time)

    def retrieve(self) -> _ImageType:
        if not self._working:
            raise FailedToFetchCameraException("Failed to retrieve frame. Did you call .open()?")
        if self._grap_call_time is None:
            raise FailedToFetchCameraException("Failed to retrieve frame. Did you call .grab()?")

        grap_call_time = self._grap_call_time
        self._grap_call_time = None

        while True:
            self._retrieve_lock.acquire()  # pylint: disable=consider-using-with
            worker_res = self._worker_res
            if worker_res[0] < 0 or isinstance(worker_res[1], str):
                raise FailedToFetchCameraException(f"Frame retrieve failed with exception: {worker_res[1]}")
            if worker_res[0] == self._target_frame:
                self._sleep_for_target_fps(grap_call_time)
                return worker_res[1]
