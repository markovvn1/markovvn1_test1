from typing import Optional, Tuple, Union

import cv2
import numpy as np
import numpy.typing as npt

from .exceptions import FailedToFetchCameraException, FailedToOpenCameraException
from .interfaces import Camera


class OpenCVCamera(Camera[npt.NDArray[np.uint8]]):
    """OpenCV camera.

    Used to work with OpenCV cameras.
    """

    _index_or_filename: Union[int, str]
    _frame_hw: Optional[Tuple[int, int]]
    _video_capture: cv2.VideoCapture

    @classmethod
    def create(cls, index_or_filename: Union[int, str], frame_hw: Optional[Tuple[int, int]] = None) -> "OpenCVCamera":
        """Create OpenCV camera.

        Args:
            index_or_filename (Union[int, str]): index or filename of the camera
            frame_hw (Tuple[int, int]): frame height and width
        """
        return cls(index_or_filename, frame_hw)

    def __init__(
        self,
        index_or_filename: Union[int, str],
        frame_hw: Optional[Tuple[int, int]] = None,
    ) -> None:
        super().__init__()
        self._index_or_filename = index_or_filename
        self._frame_hw = frame_hw
        self._video_capture = cv2.VideoCapture()

    def __del__(self) -> None:
        self.close()

    @property
    def name(self) -> str:
        return f"{self._index_or_filename}"

    @property
    def source_name(self) -> str:
        return "OpenCV"

    def is_opened(self) -> bool:
        return self._video_capture.isOpened()

    def open(self) -> None:
        if not self._video_capture.open(self._index_or_filename):
            raise FailedToOpenCameraException("Failed to open camera")
        if self._frame_hw:
            self._video_capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self._frame_hw[0])
            self._video_capture.set(cv2.CAP_PROP_FRAME_WIDTH, self._frame_hw[1])

    def close(self) -> None:
        self._video_capture.release()

    def grab(self) -> None:
        ret = self._video_capture.grab()
        if not ret:
            raise FailedToFetchCameraException("Failed to grab frame")

    def retrieve(self) -> npt.NDArray[np.uint8]:
        ret, frame = self._video_capture.retrieve()
        if not ret:
            raise FailedToFetchCameraException("Failed to retrieve frame")
        return frame
