from abc import ABC, abstractmethod
from types import TracebackType
from typing import Generic, Optional, Type, TypeVar

FrameType = TypeVar("FrameType")
T = TypeVar("T")


class Camera(ABC, Generic[FrameType]):
    def __enter__(self: T) -> T:
        return self

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc: Optional[BaseException],
        traceback: Optional[TracebackType],
    ) -> None:
        self.close()

    @property
    @abstractmethod
    def name(self) -> str:
        """Name of the camera."""

    @property
    @abstractmethod
    def source_name(self) -> str:
        """Name of the camera source."""

    @abstractmethod
    def is_opened(self) -> bool:
        """If camera opened or not."""

    @abstractmethod
    def open(self) -> None:
        """Start video streaming from the camera.

        Raises:
            FailedToOpenCameraException: failed to open camera
        """

    @abstractmethod
    def close(self) -> None:
        """Stop video streaming from the camera."""

    @abstractmethod
    def grab(self) -> None:
        """Request for the next frame.

        Raises:
            FailedToFetchCameraException: failed to grab frame
        """

    @abstractmethod
    def retrieve(self) -> FrameType:
        """Retrieve next frame and return it.

        Raises:
            FailedToFetchCameraException: failed to retrieve frame
        """
