from typing import List

import mars_cam as MVSDK
import numpy as np
import numpy.typing as npt

from .exceptions import FailedToCreateCameraException, FailedToFetchCameraException, FailedToOpenCameraException
from .interfaces import Camera


class MarsCamera(Camera[npt.NDArray[np.uint8]]):
    """Wrap for Mars cameras.

    Allows you to work with Mars series industrial cameras.
    """

    _cam: MVSDK.Camera = None
    _stream: MVSDK.StreamSource = None
    _trigSoftware: MVSDK.CmdNode = None

    @classmethod
    def discoverAll(cls) -> List["MarsCamera"]:
        """Discover all cameras.

        Have to be called in order to create cameras by their keys
        """
        return [cls(c) for c in MVSDK.System.getInstance().discovery()]

    @classmethod
    def create_by_ip(cls, ip_address: str) -> "MarsCamera":
        """Create Mars camera.

        Args:
            ip_address (str): ip address of the camera
        """
        try:
            return cls(MVSDK.System.getInstance().getCameraByIP(ip_address))
        except MVSDK.MVSDKException as e:
            raise FailedToCreateCameraException(f"Failed to create camera with ip '{ip_address}'") from e

    @classmethod
    def create_by_key(cls, key: str) -> "MarsCamera":
        """Create Mars camera.

        Args:
            key (str): unique camera key
        """
        try:
            return cls(MVSDK.System.getInstance().getCamera(key.encode()))
        except MVSDK.MVSDKException as e:
            raise FailedToCreateCameraException(f"Failed to create camera with key '{key}'") from e

    def __init__(self, cam: MVSDK.Camera) -> None:
        super().__init__()
        self._cam = cam

    def __del__(self) -> None:
        self.close()

    @property
    def name(self) -> str:
        return self.key

    @property
    def source_name(self) -> str:
        return "MarsCamera"

    @property
    def key(self) -> str:
        return self._cam.getKey().decode()

    @property
    def interface_name(self) -> str:
        return self._cam.getInterfaceName().decode()

    @property
    def vendor_name(self) -> str:
        return self._cam.getVendorName().decode()

    @property
    def model_name(self) -> str:
        return self._cam.getModelName().decode()

    @property
    def serial_number(self) -> str:
        return self._cam.getSerialNumber().decode()

    @property
    def device_version(self) -> str:
        return self._cam.getDeviceVersion().decode()

    @property
    def manufacture_info(self) -> str:
        return self._cam.getManufactureInfo().decode()

    @property
    def exposure_time(self) -> float:
        with MVSDK.DoubleNode.create(self._cam, b"ExposureTime") as exposureTimeNode:
            return exposureTimeNode.getValue()

    @exposure_time.setter
    def exposure_time(self, exposure_time: float) -> None:
        with MVSDK.DoubleNode.create(self._cam, b"ExposureTime") as exposureTimeNode:
            exposureTimeNode.setValue(exposure_time)

    def is_opened(self) -> bool:
        return self._cam.isConnect()

    def open(self) -> None:
        try:
            self._cam.connect(MVSDK.ECameraAccessPermission.accessPermissionControl)
        except MVSDK.MVSDKException as e:
            raise FailedToOpenCameraException("Failed to open camera") from e

        self._close_stream()

        try:
            with MVSDK.AcquisitionControl.create(self._cam) as acqCtrl:
                # Set trigger source to soft trigger
                with acqCtrl.triggerSource() as trigSourceEnumNode:
                    trigSourceEnumNode.setValueBySymbol(b"Software")
                with acqCtrl.triggerSelector() as trigSelectorEnumNode:
                    trigSelectorEnumNode.setValueBySymbol(b"FrameStart")
                with acqCtrl.triggerMode() as trigModeEnumNode:
                    trigModeEnumNode.setValueBySymbol(b"On")

                self._trigSoftware = acqCtrl.triggerSoftware()
        except MVSDK.MVSDKException as e:
            raise FailedToOpenCameraException("Failed to configure camera") from e

        try:
            self._stream = MVSDK.StreamSource.create(self._cam, channelId=0)
        except MVSDK.MVSDKException as e:
            raise FailedToOpenCameraException("Failed to create video stream") from e

        try:
            self._stream.startGrabbing(0, MVSDK.EGrabStrategy.grabStrartegyLatestImage)
        except MVSDK.MVSDKException as e:
            raise FailedToOpenCameraException("Failed to start video stream") from e

    def _close_stream(self) -> None:
        if self._trigSoftware:
            try:
                self._trigSoftware.release()
            except MVSDK.MVSDKException:
                pass

            self._trigSoftware = None

        if self._stream:
            try:
                self._stream.stopGrabbing()
            except MVSDK.MVSDKException:
                pass

            try:
                self._stream.release()
            except MVSDK.MVSDKException:
                pass

            self._stream = None

    def close(self) -> None:
        self._close_stream()

        try:
            self._cam.disConnect()
        except MVSDK.MVSDKException:
            pass

    def grab(self) -> None:
        if not self._trigSoftware:
            raise FailedToFetchCameraException("Failed to grab frame. Did you call .open()?")

        try:
            self._trigSoftware.execute()
        except MVSDK.MVSDKException as e:
            raise FailedToFetchCameraException("Failed to grab frame") from e

    def retrieve(self) -> npt.NDArray[np.uint8]:
        if not self._stream:
            raise FailedToFetchCameraException("Failed to retrieve frame. Did you call .open()?")

        try:
            with self._stream.getFrame(timeout_ms=1000) as frame:
                if frame.getImagePixelFormat() == MVSDK.EPixelType.gvspPixelMono8:
                    return frame.toMono8()
                return frame.toBGR24()
        except MVSDK.MVSDKException as e:
            raise FailedToFetchCameraException("Failed to retrieve frame") from e
