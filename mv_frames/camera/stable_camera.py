import time
from typing import TypeVar

from loguru import logger

from .exceptions import FailedToFetchCameraException, FailedToOpenCameraException
from .interfaces import Camera

T = TypeVar("T")


class StableCamera(Camera[T]):
    """Stable wrapper for camera.

    This wrap automatically tries to correct problems with the camera if they occur.
    """

    _instance_count: int = 0

    _sleep_time: float
    _camera: Camera[T]
    _name: str

    @classmethod
    def create(cls, camera: Camera[T], sleep_time: float = 5) -> "StableCamera[T]":
        """Create multicamera.

        Args:
            camera (Camera[T]): base camera
        """
        cls._instance_count += 1
        return cls(camera, sleep_time, f"SC{cls._instance_count}")

    def __init__(self, camera: Camera[T], sleep_time: float, name: str) -> None:
        super().__init__()
        self._sleep_time = sleep_time
        self._camera = camera
        self._name = name

    @property
    def name(self) -> str:
        return self._name

    @property
    def source_name(self) -> str:
        return "Virtual:StableCamera"

    def is_opened(self) -> bool:
        return self._camera.is_opened()

    def open(self) -> None:
        while True:
            try:
                return self._camera.open()
            except FailedToOpenCameraException:
                logger.error(
                    "Failed to open camera. Waiting {} seconds and retry",
                    self._sleep_time,
                )
                time.sleep(self._sleep_time)

    def close(self) -> None:
        self._camera.close()

    def grab(self) -> None:
        while True:
            try:
                return self._camera.grab()
            except FailedToFetchCameraException:
                logger.error("Failed to grab frame from camera. Reopenning camera")
                self.open()

    def retrieve(self) -> T:
        while True:
            try:
                return self._camera.retrieve()
            except FailedToFetchCameraException:
                logger.error("Failed to retrieve frame from camera. Reopenning camera")
                self.open()
                self.grab()
