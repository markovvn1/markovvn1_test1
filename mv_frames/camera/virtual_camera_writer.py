import json
import os
import queue
import time
from threading import Thread
from types import TracebackType
from typing import Any, Dict, List, Optional, Tuple, Type, TypeVar, Union

import cv2
import numpy as np
import numpy.typing as npt
from loguru import logger

T = TypeVar("T")

_ImageType = Union[npt.NDArray[np.uint8], List[npt.NDArray[np.uint8]]]


class VirtualCameraWriter:

    FILENAME_FORMAT = "vc_i{data_id:06d}_s{stream_id:02d}.bmp"
    EXIT_CODE = 0

    _DataWithTimestamp = Tuple[_ImageType, int]

    _working: bool = False
    _thread: Thread
    _queue: "queue.Queue[Union[int, _DataWithTimestamp]]"
    _rewrite: bool
    _folder_path: str
    _data_count: int = 0
    _data_info: Dict[str, Any]

    def __init__(self, folder_path: str, rewrite: bool = False) -> None:
        self._folder_path = os.path.abspath(folder_path)
        self._rewrite = rewrite
        self._thread = Thread(target=self._worker)
        self._queue = queue.Queue(maxsize=1)
        self._data_info = {"data": []}

        os.makedirs(self._folder_path, exist_ok=True)

    def __enter__(self: T) -> T:
        return self

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc: Optional[BaseException],
        traceback: Optional[TracebackType],
    ) -> None:
        self.close()

    def open(self) -> None:
        if self._working:
            return
        self._working = True
        self._thread.start()

    def _save_data_info(self) -> None:
        filename = os.path.join(self._folder_path, "data_info.json")
        if os.path.isfile(filename) and not self._rewrite:
            new_filename = os.path.join(self._folder_path, "data_info_old.json")
            os.replace(filename, new_filename)
            logger.warning("File with name {} already exists. Rename this file to {}", new_filename)
        with open(filename, "w", encoding="utf8") as f:
            json.dump(self._data_info, f, indent=2)

    def close(self) -> None:
        if not self._working:
            return
        self._working = False
        self._queue.put(self.EXIT_CODE)

        self._save_data_info()

        self._thread.join()

    def _save_single_image(
        self, img: npt.NDArray[np.uint8], data_id: int, stream_id: int, timestamp_ns: int
    ) -> Dict[str, Any]:
        filename = self.FILENAME_FORMAT.format(data_id=data_id, stream_id=stream_id)
        img_path = os.path.join(self._folder_path, filename)
        if os.path.isfile(img_path) and not self._rewrite:
            logger.warning("File with name {} already exists. Skip image", img_path)
        else:
            cv2.imwrite(img_path, img)
        return {
            "filename": filename,
            "timestamp_ns": timestamp_ns,
        }

    def _worker(self) -> None:
        while True:
            data = self._queue.get()
            if data == self.EXIT_CODE:
                break
            if isinstance(data, int):
                raise RuntimeError("data has incorrect type in worker")

            img_data, timestamp_ns = data

            data_info: Union[List[Dict[str, Any]], Dict[str, Any]]
            if isinstance(img_data, list):
                data_info = []
                for stream_id, img in enumerate(img_data):
                    data_info.append(self._save_single_image(img, self._data_count, stream_id, timestamp_ns))
            else:
                data_info = self._save_single_image(img_data, self._data_count, 0, timestamp_ns)

            self._data_info["data"].append(data_info)
            self._data_count += 1

    def _check_img(self, img: npt.NDArray[np.uint8]) -> None:
        if not isinstance(img, np.ndarray) or img.dtype != np.uint8:
            raise TypeError("img has incorrect type. Have to be npt.NDArray[np.uint8]")

        if (img.ndim == 2) or (img.ndim == 3 and img.shape[-1] == 3):
            return
        raise TypeError(f"img has incorrect shape. Have to be (H, W) or (H, W, 3), but img has shape {img.shape}")

    def write(self, data: Union[npt.NDArray[np.uint8], List[npt.NDArray[np.uint8]]]) -> None:
        if not self._working:
            raise RuntimeError("Writer is not working. Did you call .open()?")

        if isinstance(data, list):
            for d in data:
                self._check_img(d)
        else:
            self._check_img(data)

        self._queue.put((data, time.time_ns()))
