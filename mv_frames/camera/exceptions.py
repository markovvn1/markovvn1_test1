class CameraException(Exception):
    pass


class FailedToCreateCameraException(CameraException):
    pass


class FailedToOpenCameraException(CameraException):
    pass


class FailedToFetchCameraException(CameraException):
    pass
