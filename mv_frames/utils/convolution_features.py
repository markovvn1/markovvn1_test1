from typing import List, Tuple

import cv2

from .image_debug_logger import ImageLogger
from .image_tools import line_angle_degrees, perspective_transform, rotate_image_around, shift_image
from .mv_typing import FloatPointType, ImageType


class FeatureGroupDetector:
    def __init__(self, names: List[str], method: int = cv2.TM_SQDIFF_NORMED) -> None:
        self.detectors = []
        self.method = method
        for name in names:
            self.detectors.append(
                ConvolutionFeatureDetector(cv2.cvtColor(cv2.imread(name), cv2.COLOR_BGR2GRAY), method=method)
            )

    def do_detection(self, frame: ImageType) -> List[Tuple[FloatPointType, Tuple[FloatPointType, FloatPointType]]]:
        points = []
        for det in self.detectors:
            points.append(det.find_on_frame(frame))
        return points


class ConvolutionFeatureDetector:
    def __init__(self, feature_image: ImageType, method: int = cv2.TM_SQDIFF_NORMED) -> None:
        self.feature_image = feature_image
        ImageLogger.log_image(feature_image, "Will search for:")
        self.method = method
        self.w = self.feature_image.shape[0]
        self.h = self.feature_image.shape[1]

    def convolution(self, frame: ImageType) -> ImageType:
        return cv2.matchTemplate(frame, self.feature_image, self.method)

    def find_on_frame(self, frame: ImageType) -> Tuple[FloatPointType, Tuple[FloatPointType, FloatPointType]]:
        res = self.convolution(frame)
        _, _, min_loc, max_loc = cv2.minMaxLoc(res)
        if self.method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
            top_left = min_loc
        else:
            top_left = max_loc
        bottom_right = (top_left[0] + self.w, top_left[1] + self.h)
        return (top_left[0] + self.w / 2, top_left[1] + self.h / 2), (top_left, bottom_right)


class TemplateAlignerPerspective:
    def __init__(self, template: ImageType, feature_group_detector: FeatureGroupDetector) -> None:
        self.template = template
        self.features = feature_group_detector
        self.template_detected = self.features.do_detection(self.template)
        self.pts_nice = [i[0] for i in self.template_detected]

    def align_to_template(self, frame: ImageType) -> ImageType:
        pts = self.features.do_detection(frame)
        pts_real = [i[0] for i in pts]
        return perspective_transform(frame, pts_real, self.pts_nice)

    def align_to_template_precise(self, frame: ImageType, iterations: int = 3) -> ImageType:
        temp_frame = frame
        for _ in range(iterations):
            temp_frame = self.align_to_template(temp_frame)
        return temp_frame


class TemplateAligner:
    def __init__(self, template: ImageType, feature_group_detector: FeatureGroupDetector) -> None:
        ImageLogger.log_image(template, "Template image to be aligned: ")
        self.template = template
        self.features = feature_group_detector
        ImageLogger.log_image(template, "Searching for template features")
        self.template_detected = self.features.do_detection(self.template)
        self.pts_nice = [i[0] for i in self.template_detected]
        ImageLogger.log_image(template, "Done", self.pts_nice)

    def align_to_template(self, frame: ImageType) -> ImageType:
        ImageLogger.log_image(frame, "Searching for features")
        pts = self.features.do_detection(frame)
        pts_real = [i[0] for i in pts]
        ImageLogger.log_image(frame, "Found", pts_real)
        angles = 0.0
        for i, val in enumerate(pts_real):
            # print(line_angle_degrees([self.pts_nice[i - 1], self.pts_nice[i]], [pts_real[i - 1], val]))
            angles += line_angle_degrees([self.pts_nice[i - 1], self.pts_nice[i]], [pts_real[i - 1], val])
        angles /= len(pts_real)
        frame_final = rotate_image_around(frame, angles)

        ImageLogger.log_image(frame_final, "Rotation")
        pts = self.features.do_detection(frame)
        pts_real = [i[0] for i in pts]
        shifts = [0.0, 0.0]
        for i, val in enumerate(pts_real):
            shifts[0] += self.pts_nice[i][0] - val[0]
            shifts[1] += self.pts_nice[i][1] - val[1]
        shifts[0] /= len(pts_real)
        shifts[1] /= len(pts_real)
        res = shift_image(frame_final, shifts[0], shifts[1])
        ImageLogger.log_image(res, "Shift")
        return res

    def align_to_template_precise(self, frame: ImageType, iterations: int = 3) -> ImageType:
        ImageLogger.log_image(frame, "Iterative alignment begin")
        temp_frame = frame
        for it in range(iterations):
            temp_frame = self.align_to_template(temp_frame)
            ImageLogger.log_image(temp_frame, str(it) + " done")
        ImageLogger.log_image(temp_frame, "Alignment done")
        return temp_frame
