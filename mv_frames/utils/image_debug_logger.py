import time
from typing import List, Optional, Tuple

import cv2
import numpy as np

from mv_frames.utils.image_tools import draw_point, draw_text
from mv_frames.utils.mv_typing import ImageType
from mv_frames.utils.previewer import Previewer


class ImageLogger:
    enabled = False
    vw: Optional[Previewer] = None
    ms_last_log_time = 0.0

    @staticmethod
    def start_logging() -> None:
        ImageLogger.enabled = True
        ImageLogger.vw = Previewer("Logs")

    @staticmethod
    def stop_logging() -> None:
        ImageLogger.enabled = False
        if ImageLogger.vw is not None:
            ImageLogger.vw.stop = True

    @staticmethod
    def log_image(
        img: ImageType, text: str = "", points: Optional[List[Tuple[float, float]]] = None, delay: float = 0.5
    ) -> None:
        if not ImageLogger.enabled or ImageLogger.vw is None or (ImageLogger.vw is not None and ImageLogger.vw.stop):
            return
        if len(img.shape) == 2 or img.shape[2] == 1:
            img_copy = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        else:
            img_copy = np.copy(img)
        current_time = time.monotonic()
        if current_time - ImageLogger.ms_last_log_time < delay * 1000:
            time.sleep((delay * 1000 - (current_time - ImageLogger.ms_last_log_time)) / 1000)
        ImageLogger.ms_last_log_time = current_time
        if text != "":
            img_copy = draw_text(img_copy, text)
        if points is not None:
            for point in points:
                img_copy = draw_point(img_copy, point)
        # noinspection PyUnresolvedReferences
        ImageLogger.vw.show_images([img_copy])
