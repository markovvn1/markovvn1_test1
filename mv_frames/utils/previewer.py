import threading
from typing import List, Optional

import cv2

from .image_tools import displace_multi
from .mv_typing import FloatPointType, ImageType


class PreviewerWindow:
    def __init__(self, name: str = "Preview") -> None:
        self.press_coordinates: List[FloatPointType] = []
        self.window_name = name
        cv2.namedWindow(self.window_name, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(self.window_name, 1280, 720)
        cv2.setMouseCallback(self.window_name, self.mouse_clicked)
        self.close: bool = False

    def show(self, img: ImageType) -> None:
        if img is None:
            return
        cv2.imshow(self.window_name, img)
        val = cv2.waitKey(12)
        if val == ord("q") or self.close:
            cv2.destroyWindow(self.window_name)
            self.close = True
        elif val == ord(" "):
            self.press_coordinates = []
        elif val == ord("s"):

            self.press_coordinates = []

    def mouse_clicked(self, event: int, x: int, y: int, _flags: List[int], _parameters: List[int]) -> None:
        if event == cv2.EVENT_LBUTTONDBLCLK:
            self.press_coordinates.append((x, y))
            if len(self.press_coordinates) > 500:
                self.press_coordinates = []


class Previewer:
    pw: Optional[PreviewerWindow] = None

    def __init__(self, name: str = "Preview") -> None:
        self.performance_mode = False
        self.window_name = name
        self.showables: Optional[List[ImageType]] = None
        # self.pw: Optional[PreviewerWindow] = None
        self.stop = False
        self.performant_size = 1000
        threading.Thread(target=self.show_thread).start()

    def show_thread(self) -> None:
        self.pw = PreviewerWindow(self.window_name)
        while not self.stop:
            if self.showables is None:
                continue
            images = self.showables
            if self.performance_mode:
                image_full = displace_multi(images, size_limit=self.performant_size)
            else:
                image_full = displace_multi(images)
            # if self.performance_mode:
            self.pw.show(image_full)
            if not self.stop:
                self.stop = self.pw.close
        self.pw.close = True

    def show_images(self, images: List[ImageType], performance_mode: bool = False) -> None:
        self.showables = images
        self.performance_mode = performance_mode


class PreviewerSelector:
    def __init__(self) -> None:
        self.points: List[FloatPointType] = []
        self.img: Optional[ImageType] = None
        self.pw = Previewer()

    def show(self, image: ImageType) -> None:
        if self.pw.pw is None:
            raise ValueError("No image is being displayed, no window to track")
        self.points = self.pw.pw.press_coordinates
        self.img = image
        for point in self.pw.pw.press_coordinates:
            self.img = cv2.circle(image, (int(point[0]), int(point[1])), 10, (0, 100, 255), -1)
        if self.img is not None:
            self.pw.show_images([self.img])
