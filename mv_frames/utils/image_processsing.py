from typing import List

import cv2
import numpy as np

from mv_frames.utils.convolution_features import FeatureGroupDetector
from mv_frames.utils.image_debug_logger import ImageLogger
from mv_frames.utils.image_tools import remove_distortion
from mv_frames.utils.mv_typing import ImageType


def mv_frames_preprocess(img: List[ImageType]) -> List[ImageType]:
    n_frames = []
    for frame_index, frame in enumerate(img):
        ImageLogger.log_image(frame, "Load frame")
        frame = remove_distortion(frame, "calibration_data/calibration_data_" + str(frame_index) + ".pickle")
        ImageLogger.log_image(frame, "Unditort frame")
        brightness_threshold = np.mean(frame) - 5
        # contour_area_threshold = 10
        # contour_length_threshold = 15
        print(brightness_threshold)
        # _, thresh = cv2.threshold(frame, int(brightness_threshold), 255, cv2.THRESH_BINARY)
        frame = cv2.adaptiveThreshold(frame, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 501, 2)
        ImageLogger.log_image(frame, "Adaptive threshold")
        n_frames.append(frame)
    return n_frames


def sample_cleanup_by_features(features: FeatureGroupDetector, sample: ImageType) -> ImageType:
    ImageLogger.log_image(sample, "Sample ready, searching for features")
    pts = features.do_detection(sample)
    pts_read = [i[0] for i in pts]
    ImageLogger.log_image(sample, "Features detected", pts_read)
    mask = np.zeros((sample.shape[0] + 2, sample.shape[1] + 2), np.uint8)
    for p in pts:
        _, _, mask_2, _ = cv2.floodFill(
            sample,
            np.zeros((sample.shape[0] + 2, sample.shape[1] + 2), np.uint8),
            (int(p[0][0]), int(p[0][1])),
            0,
            0,
            0,
            cv2.FLOODFILL_FIXED_RANGE + ((8 | 255 << 8) | cv2.FLOODFILL_MASK_ONLY),
        )
        mask = cv2.bitwise_or(mask, mask_2)
    # _, img = cv2.threshold(img, 100, 255, cv2.THRESH_BINARY)
    ImageLogger.log_image(mask, "Floodfill completed")
    return mask[1:-1, 1:-1]
