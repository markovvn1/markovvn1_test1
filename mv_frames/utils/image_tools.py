import math
import pickle
from typing import List, Optional, Tuple

import cv2
import numpy as np

from .mv_typing import FloatPointType, ImageType

arucoDict_4x4_1000 = cv2.aruco.Dictionary_get(cv2.aruco.DICT_4X4_1000)
arucoParams = cv2.aruco.DetectorParameters_create()
arucoParams.cornerRefinementMethod = cv2.aruco.CORNER_REFINE_SUBPIX


def resize_with_aspect_ratio(
    img_in: ImageType, width_max: Optional[int] = None, height_max: Optional[int] = None, inter: int = cv2.INTER_AREA
) -> ImageType:
    (height_in, width_in) = img_in.shape[:2]
    r: Optional[float] = None
    if height_max is not None:
        r = height_max / float(height_in)
    if width_max is not None:
        rw = width_max / float(width_in)
        r = rw if r is None else min(r, rw)
    if r is None:
        return img_in
    return cv2.resize(img_in, (0, 0), fx=r, fy=r, interpolation=inter)


def shift_image(image: ImageType, x: float, y: float) -> ImageType:
    mat = np.array([[1, 0, x], [0, 1, y]], dtype=np.float32)
    return cv2.warpAffine(image, mat, (image.shape[1], image.shape[0]))


def rotate_point(point: FloatPointType, origin: FloatPointType, a: float) -> FloatPointType:
    x1 = point[0]
    y1 = point[1]
    x0 = origin[0]
    y0 = origin[1]
    x2 = ((x1 - x0) * math.cos(a)) - ((y1 - y0) * math.sin(a)) + x0
    y2 = ((x1 - x0) * math.sin(a)) + ((y1 - y0) * math.cos(a)) + y0
    return x2, y2


def rotate_points(pts: List[FloatPointType], origin: FloatPointType, angle: float) -> List[FloatPointType]:
    angle = math.radians(angle)
    res = []
    for point in pts:
        res.append(rotate_point(point, origin, angle))
    return res


def remove_distortion(img: ImageType, calibration_file: str) -> ImageType:
    with open(calibration_file, "rb") as handle:
        b = pickle.load(handle)
    if b is None:
        print("Cannot read a file with camera calibration parameters")
        raise FileNotFoundError
    h, w = img.shape[:2]
    # new_camera_mtx, roi = cv2.getOptimalNewCameraMatrix(b[0], b[1], (w, h), 1, (w, h))
    # dst = cv2.undistort(img, b[0], b[1], None, new_camera_mtx)
    map1, map2 = cv2.initUndistortRectifyMap(b[0], b[1], np.eye(3), b[0], (w, h), cv2.CV_16SC2)
    dst = cv2.remap(img, map1, map2, interpolation=cv2.INTER_CUBIC, borderMode=cv2.BORDER_CONSTANT)
    # x, y, w, h = roi
    # dst = dst[y:y + h, x:x + w]
    return dst


def remove_distortion_fisheye(img: ImageType, calibration_file: str) -> ImageType:
    with open(calibration_file, "rb") as handle:
        b = pickle.load(handle)
    if b is None:
        print("Cannot read a file with camera calibration parameters")
        raise FileNotFoundError
    h, w = img.shape[:2]
    map1, map2 = cv2.fisheye.initUndistortRectifyMap(b[0], b[1], np.eye(3), b[0], (w, h), cv2.CV_16SC2)
    dst = cv2.remap(img, map1, map2, interpolation=cv2.INTER_CUBIC, borderMode=cv2.BORDER_CONSTANT)
    return dst


def get_equilateral_point(a: FloatPointType, b: FloatPointType) -> FloatPointType:
    x1 = a[0]
    y1 = a[1]
    x2 = b[0]
    y2 = b[1]
    dx = x2 - x1
    dy = y2 - y1

    alpha = 60.0 / 180 * math.pi
    xp = x1 + math.cos(alpha) * dx + math.sin(alpha) * dy
    yp = y1 + math.sin(-alpha) * dx + math.cos(alpha) * dy

    return xp, yp


def dot(vec_a: FloatPointType, vec_b: FloatPointType) -> float:
    return vec_a[0] * vec_b[0] + vec_a[1] * vec_b[1]


def stack_vertical(img_list: List[ImageType]) -> ImageType:
    max_width = 0
    total_height = 0
    for img in img_list:
        if img.shape[1] > max_width:
            max_width = img.shape[1]
        total_height += img.shape[0]
    final_image = np.zeros((total_height, max_width, 3), dtype=np.uint8)

    current_y = 0
    for image in img_list:
        if max_width - image.shape[1] > 0:
            image = np.hstack((image, np.zeros((image.shape[0], max_width - image.shape[1], 3))))
        final_image[current_y : current_y + image.shape[0], :, :] = image
        current_y += image.shape[0]
    return final_image


def perspective_transform(
    image: ImageType, points_real: List[FloatPointType], points_nice: List[FloatPointType]
) -> ImageType:
    image_width = image.shape[0]
    image_height = image.shape[1]
    p_in = np.array(points_real, dtype=np.float32)
    p_out = np.array(points_nice, dtype=np.float32)
    matrix = cv2.getPerspectiveTransform(p_in, p_out)
    ret = cv2.warpPerspective(image, matrix, (image_height, image_width))
    return ret


def split_by_quarters(img: ImageType) -> List[ImageType]:
    height = img.shape[0]
    width = img.shape[1]
    width_cut = width // 2
    height_cut = height // 2
    left = img[:, :width_cut]
    right = img[:, width_cut:]
    top_left = left[height_cut:]
    bottom_left = left[:height_cut]
    top_right = right[height_cut:]
    bottom_right = right[:height_cut]
    return [top_left, top_right, bottom_left, bottom_right]


def stack_horizontal(img_list: List[ImageType]) -> ImageType:
    total_width = 0
    max_height = 0
    for img in img_list:
        if img.shape[0] > max_height:
            max_height = img.shape[0]
        total_width += img.shape[1]
    final_image = np.zeros((max_height, total_width, 3), dtype=np.uint8)
    current_y = 0  # keep track of where your current image was last placed in the y coordinate
    for image in img_list:
        if max_height - image.shape[0] > 0:
            t_img = np.zeros((max_height - image.shape[0], image.shape[1], 3))
            image = np.vstack((image, t_img))
        final_image[:, current_y : current_y + image.shape[1], :] = image
        current_y += image.shape[1]
    return final_image


def same_format(img: ImageType) -> ImageType:
    t_image = img
    if len(t_image.shape) < 3:
        t_image = cv2.cvtColor(t_image, cv2.COLOR_GRAY2BGR)
    return t_image


def line_angle_degrees(line_a: List[FloatPointType], line_b: List[FloatPointType]) -> float:
    v_a = ((line_a[0][0] - line_a[1][0]), (line_a[0][1] - line_a[1][1]))
    v_b = ((line_b[0][0] - line_b[1][0]), (line_b[0][1] - line_b[1][1]))
    angle = math.atan2(v_b[0], v_b[1]) - math.atan2(v_a[0], v_a[1])
    ang_deg = math.degrees(angle)
    return -ang_deg


def draw_point(image: ImageType, point: FloatPointType, color: Tuple[int, int, int] = (0, 64, 255)) -> ImageType:
    img = np.copy(image)

    cv2.circle(img, (int(point[0]), int(point[1])), image.shape[0] // 300 + 1, color, image.shape[0] // 100 + 1)
    return img


def displace_multi(images: List[ImageType], size_limit: Optional[int] = None) -> ImageType:
    max_size = [0, 0]
    for image in images:
        if max_size[0] < image.shape[0]:
            max_size[0] = image.shape[0]
        if max_size[1] < image.shape[1]:
            max_size[1] = image.shape[1]
    image_full = np.zeros((0, 0, 3), dtype=np.uint8)
    lines = int(math.ceil(math.sqrt(len(images))))
    cols = int(math.ceil(len(images) / lines))
    c_img = 0
    for _ in range(lines):
        hor_img = np.zeros((0, 0, 3), dtype=np.uint8)
        for _ in range(cols):
            if c_img >= len(images):
                break
            img = images[c_img]
            if img is None:
                continue
            if size_limit is not None:
                img = resize_with_aspect_ratio(
                    img,
                    width_max=int(size_limit / cols),
                    height_max=int(size_limit / lines),
                )
            else:
                img = resize_with_aspect_ratio(img, width_max=max_size[1], height_max=max_size[0])
            hor_img = stack_horizontal([hor_img, same_format(img)])
            c_img += 1
        image_full = stack_vertical([image_full, hor_img])
    return image_full


def draw_text(
    image: ImageType,
    text: str,
    text_color: Tuple[int, int, int] = (117, 255, 0),
    text_color_bg: Tuple[int, int, int] = (31, 30, 32),
) -> ImageType:
    font = cv2.FONT_HERSHEY_PLAIN
    pos = (0, 0)
    font_scale = int(image.shape[0] / 300) + 1
    font_thickness = int(image.shape[0] / 200) + 1

    x, y = pos
    text_size, _ = cv2.getTextSize(text, font, font_scale, font_thickness)
    text_w, text_h = text_size
    img = np.copy(image)
    cv2.rectangle(img, pos, (x + text_w + font_thickness // 2, y + text_h + font_thickness // 2), text_color_bg, -1)
    cv2.putText(img, text, (x, y + text_h + font_scale - 1), font, font_scale, text_color, font_thickness)
    return img


def erosion(img: ImageType, kernel_size: Tuple[int, int], iterations: int = 1) -> ImageType:
    kernel = np.ones(kernel_size, "uint8")
    erode_img = cv2.erode(img, kernel, iterations=iterations)
    return erode_img


def distance_2d(p: FloatPointType, q: FloatPointType) -> float:
    s_sq_difference = 0.0
    for p_i, q_i in zip(p, q):
        s_sq_difference += (p_i - q_i) ** 2
    distance = s_sq_difference**0.5
    return distance


def rotate_image_around(mat: ImageType, angle: float, point: Optional[FloatPointType] = None) -> ImageType:
    height, width = mat.shape[:2]
    image_center = (width / 2, height / 2)
    if point is None:
        point = image_center

    rotation_mat = cv2.getRotationMatrix2D(point, angle, 1.0)
    rotated_mat = cv2.warpAffine(mat, rotation_mat, None)
    return rotated_mat


def aruco_detection_4x4_1000(
    frame: ImageType,
) -> List[Tuple[int, Tuple[FloatPointType, FloatPointType, FloatPointType, FloatPointType], FloatPointType]]:
    (corners, ids, _) = cv2.aruco.detectMarkers(frame, arucoDict_4x4_1000, parameters=arucoParams)
    cor_by_marker = []
    if len(corners) > 0:
        ids = ids.flatten()
        for (markerCorner, markerID) in zip(corners, ids):
            corners = markerCorner.reshape((4, 2))
            (top_left, top_right, bottom_right, bottom_left) = corners
            c_x = (top_left[0] + bottom_right[0]) / 2.0
            c_y = (top_left[1] + bottom_right[1]) / 2.0
            cor_by_marker.append((markerID, (top_right, bottom_right, bottom_left, top_left), (c_x, c_y)))
    return cor_by_marker
