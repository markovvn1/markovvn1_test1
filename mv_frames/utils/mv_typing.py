from typing import Tuple

import numpy as np
import numpy.typing as ntp

ImageType = ntp.NDArray[np.uint8]
FloatPointType = Tuple[float, float]
