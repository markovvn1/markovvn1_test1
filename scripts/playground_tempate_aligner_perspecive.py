import cv2

from mv_frames.utils.convolution_features import FeatureGroupDetector, TemplateAlignerPerspective
from mv_frames.utils.previewer import Previewer

template = cv2.imread("demo_images/save2.jpg")
sample = cv2.imread("demo_images/save2_16mm.jpg")

template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
sample = cv2.cvtColor(sample, cv2.COLOR_BGR2GRAY)

features = FeatureGroupDetector(
    [
        "feature_images/f_0_4.png",
        "feature_images/f_0_5.png",
        "feature_images/f_0_6.png",
        "feature_images/f_0_7.png",
    ]
)
t = TemplateAlignerPerspective(template, features)

vw = Previewer()
img = cv2.subtract(t.align_to_template_precise(sample), template)
# _, img = cv2.threshold(img, 20, 255, cv2.THRESH_BINARY)
vw.show_images([img])
