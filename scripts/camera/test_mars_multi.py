import time

import cv2
from loguru import logger

from mv_frames.camera import MarsCamera, MultiCamera

MarsCamera.discoverAll()
camera = MultiCamera.create(
    [
        MarsCamera.create_by_key("ContrasTech:BC31596BAK00002"),
        MarsCamera.create_by_key("ContrasTech:BC31596BAK00014"),
        MarsCamera.create_by_key("ContrasTech:BC31596BAK00017"),
    ]
)
logger.info("The camera is created")

assert camera.is_opened() is False
camera.open()
assert camera.is_opened() is True
logger.info("The camera is open")

while True:
    ctime = time.monotonic()
    camera.grab()
    img = camera.retrieve()
    print("FPS: ", 1 / (time.monotonic() - ctime))

    cv2.imshow("frame1", cv2.resize(img[0], (0, 0), fx=0.1, fy=0.1))
    cv2.imshow("frame2", cv2.resize(img[1], (0, 0), fx=0.1, fy=0.1))
    cv2.imshow("frame3", cv2.resize(img[2], (0, 0), fx=0.1, fy=0.1))
    if cv2.waitKey(1) == ord("q"):
        break

camera.close()
