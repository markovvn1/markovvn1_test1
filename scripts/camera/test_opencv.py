import cv2

from mv_frames.camera import OpenCVCamera

camera = OpenCVCamera.create(2)
assert camera.is_opened() is False
camera.open()
assert camera.is_opened() is True
camera.close()
assert camera.is_opened() is False
camera.close()
assert camera.is_opened() is False
camera.close()
assert camera.is_opened() is False
camera.open()
assert camera.is_opened() is True
camera.open()
assert camera.is_opened() is True
camera.close()
assert camera.is_opened() is False
camera.open()
assert camera.is_opened() is True

while True:
    camera.grab()
    img = camera.retrieve()
    cv2.imshow("frame", img)
    if cv2.waitKey(1) == ord("q"):
        break

camera.close()
