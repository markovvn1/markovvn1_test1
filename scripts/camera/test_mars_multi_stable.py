import time
from typing import List

import cv2
import numpy as np
import numpy.typing as npt
from loguru import logger

from mv_frames.camera import Camera, MarsCamera, MultiCamera, StableCamera
from mv_frames.camera.exceptions import FailedToCreateCameraException


def create() -> Camera[List[npt.NDArray[np.uint8]]]:
    while True:
        MarsCamera.discoverAll()
        try:
            res = StableCamera.create(
                MultiCamera.create(
                    [
                        MarsCamera.create_by_key("ContrasTech:BC31596BAK00002"),
                        MarsCamera.create_by_key("ContrasTech:BC31596BAK00014"),
                        MarsCamera.create_by_key("ContrasTech:BC31596BAK00017"),
                    ]
                )
            )
            logger.info("The camera is created")
            return res
        except FailedToCreateCameraException:
            logger.error("Failed to create camera. Waiting 5 seconds and retry")
            time.sleep(5)


with create() as camera:
    camera.open()
    logger.info("The camera is open")

    while True:
        ctime = time.monotonic()
        camera.grab()
        img = camera.retrieve()
        print("FPS: ", 1 / (time.monotonic() - ctime))

        cv2.imshow("frame1", cv2.resize(img[0], (0, 0), fx=0.1, fy=0.1))
        cv2.imshow("frame2", cv2.resize(img[1], (0, 0), fx=0.1, fy=0.1))
        cv2.imshow("frame3", cv2.resize(img[2], (0, 0), fx=0.1, fy=0.1))
        if cv2.waitKey(1) == ord("q"):
            break

    # automatic camera close
