import cv2
from loguru import logger

from mv_frames.camera import MarsCamera, MultiCamera, VirtualCamera

MarsCamera.discoverAll()
camera = MultiCamera.create(
    [
        MarsCamera.create_by_key("ContrasTech:BC31596BAK00002"),
        MarsCamera.create_by_key("ContrasTech:BC31596BAK00014"),
        MarsCamera.create_by_key("ContrasTech:BC31596BAK00017"),
    ]
)
logger.info("The camera is created")


with camera, VirtualCamera.Writer("2", rewrite=True) as cameraWriter:
    camera.open()
    cameraWriter.open()
    logger.info("The camera is open")

    while True:
        camera.grab()
        img = camera.retrieve()
        cameraWriter.write(img)

        cv2.imshow("frame1", cv2.resize(img[0], (0, 0), fx=0.1, fy=0.1))
        cv2.imshow("frame2", cv2.resize(img[1], (0, 0), fx=0.1, fy=0.1))
        cv2.imshow("frame3", cv2.resize(img[2], (0, 0), fx=0.1, fy=0.1))
        if cv2.waitKey(1) == ord("q"):
            break
