import cv2

from mv_frames.camera import MarsCamera

MarsCamera.discoverAll()
camera = MarsCamera.create_by_key("ContrasTech:BC31596BAK00002")

print("Key           = " + str(camera.key))
print("Vendor name   = " + str(camera.vendor_name))
print("Model  name   = " + str(camera.model_name))
print("Serial number = " + str(camera.serial_number))

assert camera.is_opened() is False
camera.open()
assert camera.is_opened() is True
camera.close()
assert camera.is_opened() is False
camera.close()
assert camera.is_opened() is False
camera.close()
assert camera.is_opened() is False
camera.open()
assert camera.is_opened() is True
camera.open()
assert camera.is_opened() is True
camera.close()
assert camera.is_opened() is False
camera.open()
assert camera.is_opened() is True

print("Exposure time = " + str(camera.exposure_time))
camera.exposure_time = 80000
print("Exposure time = " + str(camera.exposure_time))

while True:
    camera.grab()
    img = camera.retrieve()
    cv2.imshow("frame", img)
    if cv2.waitKey(1) == ord("q"):
        break

camera.close()
