import time

import cv2

from mv_frames.camera import MultiCamera, OpenCVCamera
from mv_frames.camera.exceptions import FailedToFetchCameraException, FailedToOpenCameraException

camera = MultiCamera.create([OpenCVCamera(0), OpenCVCamera(2)])
assert camera.is_opened() is False
camera.open()
assert camera.is_opened() is True
camera.close()
assert camera.is_opened() is False
camera.close()
assert camera.is_opened() is False
camera.close()
assert camera.is_opened() is False
camera.open()
assert camera.is_opened() is True
camera.open()
assert camera.is_opened() is True
camera.close()
assert camera.is_opened() is False
camera.open()
assert camera.is_opened() is True

while True:
    try:
        camera.grab()
        img = camera.retrieve()
    except FailedToFetchCameraException:
        while True:
            try:
                camera.close()
                camera.open()
                break
            except FailedToOpenCameraException:
                time.sleep(1)
    cv2.imshow("frame1", img[0])
    cv2.imshow("frame2", img[1])
    if cv2.waitKey(1) == ord("q"):
        break

camera.close()
