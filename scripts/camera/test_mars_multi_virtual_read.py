import cv2

from mv_frames.camera import VirtualCamera

with VirtualCamera.create("2", target_fps=30, loop_mode=VirtualCamera.LoopMode.LOOP) as camera:
    camera.open()

    while True:
        camera.grab()
        img = camera.retrieve()
        cv2.imshow("frame1", cv2.resize(img[0], (0, 0), fx=0.1, fy=0.1))
        cv2.imshow("frame2", cv2.resize(img[1], (0, 0), fx=0.1, fy=0.1))
        cv2.imshow("frame3", cv2.resize(img[2], (0, 0), fx=0.1, fy=0.1))
        if cv2.waitKey(1) == ord("q"):
            break
