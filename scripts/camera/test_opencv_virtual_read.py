import cv2

from mv_frames.camera import VirtualCamera

with VirtualCamera.create("1", target_fps=30, loop_mode=VirtualCamera.LoopMode.LOOP) as camera:
    camera.open()

    while True:
        camera.grab()
        img = camera.retrieve()
        cv2.imshow("frame", img)
        if cv2.waitKey(1) == ord("q"):
            break
