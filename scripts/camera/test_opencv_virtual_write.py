import cv2

from mv_frames.camera import OpenCVCamera, VirtualCamera

with OpenCVCamera.create(0) as camera, VirtualCamera.Writer("1", rewrite=True) as cameraWriter:
    camera.open()
    cameraWriter.open()

    while True:
        camera.grab()
        img = camera.retrieve()
        cameraWriter.write(img)  # save image to file
        cv2.imshow("frame", img)
        if cv2.waitKey(1) == ord("q"):
            break
