import sys
import time
from typing import List

import cv2
import numpy as np
import numpy.typing as npt
from loguru import logger

from mv_frames.camera import Camera, MarsCamera, MultiCamera, StableCamera
from mv_frames.camera.exceptions import FailedToCreateCameraException
from mv_frames.utils.image_processsing import mv_frames_preprocess
from mv_frames.utils.previewer import Previewer


def create() -> Camera[List[npt.NDArray[np.uint8]]]:
    while True:
        MarsCamera.discoverAll()
        try:
            res = StableCamera.create(
                MultiCamera.create(
                    [
                        MarsCamera.create_by_key("ContrasTech:BC31596BAK00002"),
                        MarsCamera.create_by_key("ContrasTech:BC31596BAK00017"),
                        MarsCamera.create_by_key("ContrasTech:BC31596BAK00014"),
                    ]
                )
            )
            logger.info("The camera is created")
            return res
        except FailedToCreateCameraException:
            logger.error("Failed to create camera. Waiting 5 seconds and retry")
            time.sleep(5)


with create() as camera:
    camera.open()
    logger.info("The camera is open")
    vw = Previewer()
    while True:
        ctime = time.monotonic()
        camera.grab()
        img = mv_frames_preprocess(camera.retrieve())
        cv2.imwrite("snapshot_images/snapshot_0.jpg", img[0])
        cv2.imwrite("snapshot_images/snapshot_1.jpg", img[1])
        cv2.imwrite("snapshot_images/snapshot_2.jpg", img[2])
        print("FPS: ", 1 / (time.monotonic() - ctime))
        vw.show_images(img)
        if vw.stop:
            sys.exit()
    # automatic camera close
