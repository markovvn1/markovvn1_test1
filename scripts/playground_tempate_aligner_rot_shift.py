import cv2

from mv_frames.utils.convolution_features import FeatureGroupDetector, TemplateAligner
from mv_frames.utils.image_debug_logger import ImageLogger
from mv_frames.utils.image_processsing import mv_frames_preprocess, sample_cleanup_by_features
from mv_frames.utils.image_tools import erosion

ImageLogger.start_logging()

template = cv2.imread("feature_images/ref_0.jpg")
sample = cv2.imread("snapshot_images/snapshot_0.jpg")

template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
sample = cv2.cvtColor(sample, cv2.COLOR_BGR2GRAY)
sample = mv_frames_preprocess([sample])[0]

features = FeatureGroupDetector(
    [
        "feature_images/f_0_0.jpg",
        "feature_images/f_0_1.jpg",
        "feature_images/f_0_2.jpg",
        "feature_images/f_0_3.jpg",
        "feature_images/f_0_4.jpg",
        "feature_images/f_0_5.jpg",
        "feature_images/f_0_6.jpg",
    ]
)

sample = sample_cleanup_by_features(features, sample)
t = TemplateAligner(template, features)

img = cv2.subtract(t.align_to_template_precise(sample), template)
ImageLogger.log_image(img, "diff between template and sample")
img = erosion(img, (4, 5))
ImageLogger.log_image(img, "erosion")
contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
cv2.drawContours(img, contours, -1, (0, 0, 255), 30)
ImageLogger.log_image(img, "contours detection")
# vw = Previewer()
# vw.show_images([img])
