<div align="center">
  <img src=".gitlab/logo.png" height="124px"/><br/>
  <h1>MV Frames</h1>
  <p>Frame scanning and analysis</p>
</div>



## 📝 About The Project
The project is being commissioned by *placeholder*. The main task of the project is to check that the finished parts correspond to the drawing and assess the quality of these parts.

## ⚡️ Quick start

Download the repository and change the current directory:

```bash
git clone git@gitlab.com:mirai-vision/projects/mv-frames.git && cd mv-frames
```

Configure virtual environment. Make sure a `.venv` folder has been created after this step.

```bash
make venv
# source .venv/bin/activate
```

Run application:

```bash
make up
```

## ⚙️ Developing

Run linters, formaters and tests:
```bash
make lint  # check code quality
make format  # beautify code
make test  # run tests
```

Switch to local version of *mirai* package (so you can modify both packages together):

```bash
# clone mirai so that it is flush with the current project
cd .. && git clone git@gitlab.com:mirai-vision/mirai.git
# go back to your project
cd mv-frames
# switch to local version of mirai package
make update_mirai path=../mirai
```

Switch back to the stable version of *mirai* package:

```bash
make update_mirai version=<version>
# example: make update_mirai version=0.2.0b0
```

## :computer: Contributors

<p>

  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>Markovvn1@gmail.com</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>
</p>