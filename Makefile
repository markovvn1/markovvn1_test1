VENV ?= .venv
CODE = mv_frames scripts tests

.PHONY: venv
venv:
	python -m venv $(VENV)
	$(VENV)/bin/python -m pip install --upgrade pip
	$(VENV)/bin/python -m pip install poetry==1.2.0b2
	$(VENV)/bin/poetry install

.PHONY: test
test:
	$(VENV)/bin/pytest -v tests

.PHONY: lint
lint:
	$(VENV)/bin/flake8 --jobs 4 --statistics --show-source $(CODE)
	$(VENV)/bin/pylint --jobs 4 $(CODE)
	$(VENV)/bin/mypy $(CODE)
	$(VENV)/bin/black --skip-string-normalization --check $(CODE)

.PHONY: format
format:
	$(VENV)/bin/isort $(CODE)
	$(VENV)/bin/black $(CODE)
	$(VENV)/bin/pautoflake --recursive --remove-all-unused-imports --in-place $(CODE)

.PHONY: ci
ci:	lint test

.PHONY: update_mirai
update_mirai:
	[ -n "$(value version)" ] && poetry add --source mirai mirai==$(value version) || [ -n "$(value path)" ] 
	[ -n "$(value path)" ] && poetry add --editable $(value path) || [ -n "$(value version)" ]

.PHONY: up
up:
	$(VENV)/bin/python -m mv_frames
